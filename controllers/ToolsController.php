<?php

namespace app\controllers;

use api\models\DatabaseList;
use api\models\DatamartDb;
use api\models\Encryptor;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `api` module
 */
class ToolsController extends Controller
{
    public static function allowedDomains()
    {
        return [
            'http://localhost:8888',
        ];
    }
    public function behaviors()
    {
        return [
            [
                "class" => \yii\filters\ContentNegotiator::className(),
                "only" => ["tabledata", "tableschema", "test2"],
                "formats" => [
                    "application/json" => \yii\web\Response::FORMAT_JSON,
                ],

            ],
            "corsFilter" => [
                "class" => \yii\filters\Cors::className(),
                "cors" => [
                    "Origin" => static::allowedDomains(),
                    "Access-Control-Request-Method" => ['POST', 'GET'],
                    "Access-Control-Allow-Credentials" => true,
                    "Access-Control-Max-Age" => 3600,
                ],
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPostjson()
    {
        return $this->render("postjson");
    }

    public function actionDatabrowser()
    {

        $dbList = DatabaseList::find()->all();

        return $this->render("databrowser", [
            "databases" => $dbList,

        ]);
    }
    public function actionDatabase($name)
    {
        $name = base64_decode($name);
        $dbname = Encryptor::decrypt($name);

        if (!$this->isValidName($dbname)) {
            return $this->render("databasenotfound", [
                "databaseName" => $dbname,
            ]);
        }
        //  print_r($dbname);exit;
        $dbmart = new DatamartDb($dbname, false);
        if ($dbmart->isExists == false) {
            return $this->render("databasenotfound", [
                "databaseName" => $dbname,
            ]);
        }
        $tableNames = $dbmart->schema->getTableNames();
        $firsttable = null;
        if (sizeof($tableNames) > 0) {
            $firsttable = $tableNames[0];
        }

        return $this->render("database", [
            "tableNames" => $tableNames,
            "databaseName" => $dbname,
        ]);

    }

    public function actionTabledata($name)
    {
        $name = base64_decode($name);
        $parameters = Encryptor::decrypt($name);
        $pattern = "/db:(?<db>[a-zA-Z0-9_$]+);tbl:(?<tbl>[a-zA-Z0-9_$]+)/i";
        $matches = array();
        preg_match($pattern, $parameters, $matches);
        $dbname = array_key_exists("db", $matches) ? $matches["db"] : null;
        $tablename = array_key_exists("db", $matches) ? $matches["tbl"] : null;

        if ($dbname == null || $tablename == null) {
            return ["success" => false,
                "data" => "<strong>Oops!</strong>  Something wrong occured: table not found."];
        }
        if (!$this->isValidName($dbname) && !$this->isValidName($dbname)) {
            return ["success" => false,
                "data" => "<strong>Oops!</strong>  Something wrong occured: table not found."];
        }
        $db = new DatamartDb($dbname);

        $query = (new \yii\db\Query());
        $result = $query->from($tablename)->all($db);
        return ["data" => $result, "success" => true];
    }

    public function actionTableschema($name)
    {
        $name = base64_decode($name);
        $parameters = Encryptor::decrypt($name);
        $pattern = "/db:(?<db>[a-zA-Z0-9_$]+);tbl:(?<tbl>[a-zA-Z0-9_$]+)/i";
        $matches = array();
        preg_match($pattern, $parameters, $matches);
        $dbname = array_key_exists("db", $matches) ? $matches["db"] : null;
        $tablename = array_key_exists("db", $matches) ? $matches["tbl"] : null;

        if ($dbname == null || $tablename == null) {
            return ["success" => false,
                "data" => "<strong>Oops!</strong>  Something wrong occured: table not found."];
        }
        if (!$this->isValidName($dbname) && !$this->isValidName($dbname)) {
            return ["success" => false,
                "data" => "<strong>Oops!</strong>  Something wrong occured: table not found."];
        }
        $db = new DatamartDb($dbname);
        $tableSchema = $db->schema->getTableSchema($tablename);
        $tablecolumns = array_keys($tableSchema->columns);
        return ["success" => true, "data" => $tablecolumns];
    }

    private function isValidName($text)
    {
        return preg_match('/^[0-9a-zA-Z$_]+$/i', $text) != 0;
    }

    public function actionTest2()
    {

        $arr = ["2005-01-01 12:00", "2005-01-01 13:00", "2005-01-01", "2005-01-01 23:59"];
        $arr = ["123.3500", "355.00", "123.534"];
        $arr = ["true", "false", "true"];
        return $this->getColumnType($arr);
    }

    private function getColumnType($arr)
    {
        $size = sizeof($arr);
        $size = $size > 100 ? 100 : $size;
        $test_array = array_slice($arr, 0, $size);

        $date_pattern = '/^\d{4}[\-.]\d{1,2}[\-.]\d{1,2}$/';
        $datetime_pattern = '/^\d{4}[\-.]\d{1,2}[\-.]\d{1,2}\s\d{1,2}:\d{1,2}[:\d{1,2}]$/';
        $float_pattern = '/^\d+\.\d+$/i';
        $int_pattern = '/^\d+$/i';
        $boolean_pattern = '/^true|false$/i';

        $patterns = ["datetime" => $datetime_pattern, "date" => $date_pattern,
            "float" => $float_pattern, "int" => $int_pattern,
            "boolean" => $boolean_pattern];

        $result = '';
        $presult = [];
        foreach ($patterns as $pattern => $value) {
            $grep = preg_grep($value, $test_array);
            $presult[$pattern] = $grep;

            if (sizeof($grep) == $size) {
                $result = $pattern;
                break;
            }

        }

        if ($result == '') {
            $result = 'string';
        }

        return $presult;

    }

}
