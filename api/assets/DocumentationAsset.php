<?php
namespace api\assets;

use yii\web\AssetBundle;

class DocumentationAsset extends AssetBundle
{

    // The directory that contains the source asset files for this asset bundle
    public $sourcePath = '@api/web/';

    // List of CSS files that this bundle contains
    public $css = ['css/documentstyle.css'];
  
  
}
