<?php
/**
 * this config should be merge with 
 * web\index.php
 */
return [

    'components' => [
        'urlManager' => [
            'rules' => [
                'api/v1/barcodes/'=>'api/v1/barcode/index',
                'api/v1/barcode/<id:\d+>'=>'api/v1/barcode/view',
                'api/v1/barcodes/page/<page:\d+>'=>'api/v1/barcode/index',
            ],
        ],
    ],

];
