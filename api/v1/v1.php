<?php

namespace api\v1;

/**
 * api module definition class
 */
class v1 extends \yii\base\Module
{
    public $controllerNamespace = 'api\v1\controllers';
    public function init()
    {
        parent::init();
    }
}
