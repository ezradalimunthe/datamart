<?php

namespace api\v1\controllers;

use yii\helpers\Url;

class BarcodeController extends \yii\rest\ActiveController
{

    public $modelClass = 'app\models\Barcodes';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'barcodes',
    ];
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'only' => ['index', 'view'],
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
  public function actionTest(){
      echo "test";exit;
  }
}
