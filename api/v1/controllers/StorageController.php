<?php

namespace api\v1\controllers;

use api\models\JsonRecordset;
use Yii;
use yii\web\Controller;

class StorageController extends Controller
{
    public function behaviors()
    {
        return [
            [
                "class" => \yii\filters\ContentNegotiator::className(),
                //"only" => ["index", "view"],
                "formats" => [
                    "application/json" => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {

        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }
    public function actionIndex()
    {

        $request = Yii::$app->request;
        if (!$request->isPost) {

            throw new \Exception("Request must be post");
            exit;
        }
        $json = Yii::$app->getRequest()->getBodyParams();
        $jsRecordset = new JsonRecordset();
        $result = $jsRecordset->insertRecord($json);

        return $result;
    }

    public function actionTest()
    {
        $column = Yii::$app->db->schema->createColumnSchemaBuilder("string", 20);
        $column->notNull()->append("Auto_increment");

        return $column->__toString();
    }
    public function actionTest2()
    {
        $arr = [];
        for ($i = 0; $i < 4; $i++) {
            $arr[] = [
                "key" => "Error",
                "value" => $i,
            ];
        }
        $arr2 = [];
        for ($i = 10; $i < 14; $i++) {
            $arr2[] = [
                "key" => "Error",
                "value" => $i,
            ];
        }
        return array_merge($arr, $arr2);
    }
}
