<?php 

namespace api\v1\controllers;

use Yii;

use yii\web\Controller;
use api\models\Document;

class DocumentationController extends Controller
{
    public function actionIndex()
    {
        $alldocs = Document::getBooks();

        $navs = array_map(function ($item) {
            $title  = preg_split('/(?=[A-Z])/',$item);
            $title = implode(" ", $title);
            return ["label" => $title, "url" => ["documentation/kb/", "book" => $item]];
        }, $alldocs);

        return $this->render('index', ["navs" => $navs]);
    }

    public function actionKb($book, $page = null)
    {

        $allfiles = Document::getPages($book);
        $navs = array_map(function ($item) use ($book, $page) {
            $item = basename($item, ".md");
            $title  = preg_split('/(?=[A-Z])/',$item);
            $title = implode(" ", $title);
            return ["label" => $title,
                "url" => ["documentation/kb/", "book" => $book, "page" => $item],
                'active' => $item == $page,
            ];
        }, $allfiles);
        $content = "##No content found for this book.";
        if ($page != null) {
            $content = Document::getPage($book, $page);
        } else {
            if ($allfiles && sizeof($allfiles) > 0) {
                $content = Document::getPage($book, basename($allfiles[0], ".md"));
                $navs[0]['active'] = true;
            }
        }
        return $this->render('kb', [
            "pages" => $navs,
            "book" => $book,
            "currentPage" => $content,
        ]);
    }
}