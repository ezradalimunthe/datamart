<?php
/* @var $this yii\web\View */
use api\assets\DocumentationAsset;
use yii\helpers\Html;
DocumentationAsset::register($this);
?>
<h3>Documentation/index</h3>
<?php
foreach ($navs as $nav) {?>
<div class="card" style="width: 18rem;">
    <?=Html::img("/img/book.jpg", ["class" => "card-img-top", "alt" => $nav["label"]]);?>

    <div class="card-body">
        <h5 class="card-title"><?=$nav["label"]?></h5>
        <p class="card-text">
            <?=Html::a("Open", $nav["url"]);?>
       </p>
    </div>
</div>
<?php }?>