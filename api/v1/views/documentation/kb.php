<?php

use yii\bootstrap4\Nav;

$this->title = $book;
use yii\helpers\Markdown;
use api\assets\DocumentationAsset;
use yii\helpers\Html;
DocumentationAsset::register($this);
?>



<div class="content">
    <div class="row">
        <div class="col-sm-2 border-right">
            <h3><?=$book;?></h3>
            <button class="btn btn-primary d-sm-none" type="button"
                onclick="$('#kb_menu').slideToggle( 'slow',function(){$('#kb_menu').toggleClass('show').css({'display':''})});"
                aria-expanded="true" aria-controls="collapseOne">
                Content
            </button>
            <div id="kb_menu">
                <?=Nav::widget([
    'items' => $pages,
    'options' => ["class" => "nav flex-column kb-navigation"],
]);?>
            </div>
        </div>
        <div class="col-sm-10">
            <div class="kb-content">
                <?=Markdown::process($currentPage, "gfm");?>
            </div>

        </div>
    </div>
</div>