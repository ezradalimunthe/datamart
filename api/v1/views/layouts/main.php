<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\components\Navbar;
use yii\bootstrap4\Nav;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">

<head>
    <meta charset="<?=Yii::$app->charset?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no"
        name="viewport">
    <?php $this->registerCsrfMetaTags()?>
    <title>API <?=Html::encode($this->title)?></title>
    <?php $this->head()?>
</head>

<body class="index-page sidebar-collapse">
    <?php $this->beginBody()?>


    <?php

    
NavBar::begin([
    'brandLabel' => "API " . Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'id' => 'topnavbar',

    'options' => [
        'class' => 'navbar navbar-expand-lg bg-primary fixed-top ',
        'data-color-on-scroll' => "400",
    ],
    'collapseOptions' => ['id' => 'navigation', 'class' => 'collapse navbar-collapse justify-content-end'],
    'togglerContent' => implode("", [
        '<span class="navbar-toggler-bar top-bar"></span>',
        '<span class="navbar-toggler-bar middle-bar"></span>',
        '<span class="navbar-toggler-bar bottom-bar"></span>',
    ]),

]);
$menuItems = [
    
    ['label' => 'Documentation', 'url' => ['/api/v1/documentation/index']],
   
    
];

echo Nav::widget([
    'activateParents' => true,
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);

NavBar::end();
?>
    <div class="wrapper">
        <div class="main">
            <?=$content?>
        </div>
        <footer class="footer  bg-dark text-white">
            <div class="container">
                <div class="row">
                    <div class='col-sm-8'>
                        <address>
                            <strong>PT. Halotec Indonesia</strong><br>
                            Jalan Danau Sentani No 7<br>
                            Medan, Sumatera Utara<br>
                            Indonesia <br>
                            <abbr title="Phone">P:</abbr> (123) 456-7890
                        </address>

                    </div>
                    <div class="col-sm-4">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <?=Html::a("About", ["site/about"]);?>

                            </li>
                            <li class="nav-item"> <?=Html::a("Contact", ["site/contact"]);?></li>

                            <li class="nav-item"></li>
                        </ul>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        &copy; <?=Html::encode(Yii::$app->name)?> <?=date('Y')?>
                    </div>
                </div>

            </div>
        </footer>
    </div>



    <?php $this->endBody()?>

</body>

</html>
<?php $this->endPage()?>