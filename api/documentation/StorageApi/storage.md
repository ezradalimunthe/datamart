# Storage

1. Method: POST
2. URL: /api/v1/storage
3. Content-type: application/json
4. Json Structure

## JSON Structure

| Property | Type | Description | Mandatory |
| ----------- | ----------- |------|------------|
| id | string | database name | Yes|
| owner | string (100) | database owner | No |
| description | string (255)| short text explain the database | No|
| tables | array of table structure | | Yes |

## Table Structure

 

| Property | Type | Description | Mandatory |
| ----------- | ----------- |------|------------|
| id | string | table name | Yes|
| alias | string | table alias | No|
| primaryKey | string | table primary key, value: valid column name | no |
| columns | array of column structure | | optional |
| data | array of data | | optional |

## Column Structure

Bila Column Structure ada pada struktur tabel, maka system akan 
mencoba membuat table dengan column-column yang disediakan setelah mengecek apakah tabel dengan nama yang sama ada pada database.


| Property | Type | Description | Mandatory |
| ----------- | ----------- |------|------------|
| id | string | column name | Yes|
| dataType | string | data type | Yes|
| size | int | column data size | no |

## Data Structure

Data structure digunakan untuk memasukkan data pada tabel yang telah ditentukan. Nama-nama property pada data structure harus sama dengan nama kolom di database.


Possible Column Structure dataType:

1. int, integer, bigint
2. string, char, text
3. datetime, timestamp
4. float, decimal, money
5. binary

##Example

<pre class="border">
<code >{
  "id": "covid19_februari",
  "owner": "ezra@halotec-indonesia.com",
  "description": "Data Covid-19 berdasarkan flyer dari Kemenkes",
  "tables": [
    {
      "id": "indonesia",
      "columns": [
        { "id": "record_date", "dataType": "date" },
        { "id": "positif", "dataType": "int" },
        { "id": "sembuh", "dataType": "int" },
        { "id": "meninggal", "dataType": "int" },
        { "id": "aktif", "dataType": "int" },
        { "id": "suspek", "dataType": "int" },
        { "id": "spesimen", "dataType": "int" }
      ],
      "data": [
        {
          "record_date": "2021-02-01",
          "positif": 1089308,
          "sembuh": 8883682,
          "meninggal": 30227,
          "aktif": 175349,
          "suspek": 76343,
          "spesimen": 48213
        },
        {
          "record_date": "2021-02-02",
          "positif": 1099687,
          "sembuh": 896530,
          "meninggal": 30581,
          "aktif": 172675,
          "suspek": 75333,
          "spesimen": 71702
        },
        {
          "record_date": "2021-02-03",
          "positif": 1111671,
          "sembuh": 905665,
          "meninggal": 30770,
          "aktif": 175236,
          "suspek": 76657,
          "spesimen": 74965
        }
      ]
    }
  ]
}
</code>
</pre>
