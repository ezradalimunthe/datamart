<?php

namespace api\models;

class Encryptor
{

    private const CHIPERING = "AES-128-CTR";
    private const ENCRYPTION_IV = '81277763016EZRAD';
    private const ENCRYPTION_KEY = "halotecindonesia";
    public function __construct(Type $var = null)
    {
        $this->var = $var;
    }
    public static function encrypt($text)
    {
        return openssl_encrypt($text, self::CHIPERING,
            self::ENCRYPTION_KEY, 0, self::ENCRYPTION_IV);

    }
    public static function decrypt($text)
    {
        return openssl_decrypt($text, self::CHIPERING,
            self::ENCRYPTION_KEY, 0, self::ENCRYPTION_IV);
    }
}
