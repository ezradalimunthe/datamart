<?php
namespace api\models;

/**
 * purpose: create table on
 */
use api\models\DatabaseList;
use api\models\DatamartDb;

class JsonRecordset
{
    const MESSAGE_TYPE_ERROR = "error";
    const MESSAGE_TYPE_INFO = "info";
    const MESSAGE_TYPE_WARNING = "warning";

    /**
     * $json : array
     */
    public $db = null;
    private $messages = [];
    public function insertRecord($json)
    {
        $hasId = true;
        $hasTables = true;

        //json must have id and tables properties

        $hasId = array_key_exists("id", $json);
        $hasTables = array_key_exists("tables", $json);

        if (!($hasId)) {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_ERROR,
                "message" => "Json must have database id property.",
            ];
            return $this->messages;
        }
        $databaseId = $json["id"];

        if (!$this->validateName($databaseId)) {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_ERROR,
                "message" => "Database name is not valid. Only Alphanumeric, underscore and dollar sign allowed.",
            ];
            return $this->messages;
        }

        //set
        $this->db = new DatamartDB($databaseId);

        $dbinfo = DatabaseList::find()->where([
            "database_name" => "datamart_" . $databaseId,
        ])->one();
        if ($dbinfo == null) {
            $dbinfo = new DatabaseList();
        }

        //update database info when description or owner changed.
        $dbinfo->owner = array_key_exists("owner", $json) ? $json['owner'] : null;
        $dbinfo->description = array_key_exists("description", $json) ? $json['description'] : null;
        if ($dbinfo->owner != null || $dbinfo->description != null) {
            $dbinfo->save();
        }

        if (!($hasTables)) {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_ERROR,
                "message" => "No tables found.",
            ];
            return $this->messages;
        }
        //table must have id and columns or data property
        $tableProcessed = 0;
        $tableIndex = 0;
        foreach ($json['tables'] as $table) {
            $tableIndex++;
            $hasId = array_key_exists("id", $table);
            $hasTableColumns = array_key_exists("columns", $table);
            $hasTableData = array_key_exists("data", $table);
            if ($hasId == false) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_ERROR,
                    "message" => "A table has no id so no action will be taken.",
                ];
                continue;
            }

            $tableId = $table["id"];
            if (!$this->validateName($tableId)) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_ERROR,
                    "message" => "Table name '$tableId' is not valid. Only Alphanumeric, underscore and dollar sign allowed.",
                ];
                continue;
            }

            $tableSchema = $this->db->schema->getTableSchema($tableId);

            if ($hasTableColumns && $tableSchema != null) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_INFO,
                    "message" => "Table $tableId has already in database so will not be created.",
                ];

            } else if ($hasTableColumns && $tableSchema == null) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_INFO,
                    "message" => "Begin Table $tableId creation",
                ];
                $primaryKey = null;
                if (array_key_exists("primaryKey", $table)) {
                    $primaryKey = $table["primaryKey"];
                }
                $result = $this->createTable($tableId, $table['columns'], $primaryKey);

                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_INFO,
                    "message" => "End Table $tableId creation.",
                ];
            } else if ($hasTableData && $hasTableColumns == false && $tableSchema == null) {

                $columns = $this->createColumnDefs($table['data']);
                $result = $this->createTable($tableId, $columns);

            }

            //double check

            if ($tableSchema == null) {
                $tableSchema = $this->db->schema->getTableSchema($tableId);
            }

            if ($hasTableData) {
                $result = $this->insertData($table['data'], $tableSchema);

            }
            $tableProcessed++;
        } //foreach table

        if ($tableProcessed == 0) {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_WARNING,
                "message" => "No table is processed!",
            ];
        } else {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_INFO,
                "message" => "Tables processed: " . $tableProcessed,
            ];
        }
        return $this->messages;

    }
    private function createTable($tablename, $columns, $primaryKey = null)
    {
        $this->messages[] = [
            "type" => self::MESSAGE_TYPE_INFO,
            "message" => "Begin Table $tableId creation",
        ];
        $columnDefs = array();
        $returnvalue = true;
        $hasError = false;
        $colIndex = 0;

        foreach ($columns as $col) {
            if (array_key_exists("id", $col) == false) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_ERROR,
                    "message" => "Column at index $colIndex has no id",
                ];
                $hasError = $hasError || true;
            }
            if (array_key_exists("dataType", $col) == false) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_ERROR,
                    "message" => "Column at index $colIndex has no dataType",
                ];
                $hasError = $hasError || true;
            }
            if (array_key_exists("id", $col)) {
                if (!$this->validateName($col['id'])) {
                    $this->messages[] = [
                        "type" => self::MESSAGE_TYPE_ERROR,
                        "message" => "Column name '" . $col['id'] . "' is not valid. Only Alphanumeric, " .
                        "underscore and dollar sign allowed.",
                    ];
                    $hasError = $hasError || true;
                }
            }

            $datatype = $col['dataType'];
            $size = array_key_exists("size", $col) ? $col["size"] : null;
            $allowNull = array_key_exists("allowNull", $col) ? $col["allowNull"] : true;

            $auto_increment = array_key_exists("autoIncrement", $col) ?
            $col["autoIncrement"] == true : null;

            $columndef = $this->db->schema->createColumnSchemaBuilder($datatype, $size);
            if ($auto_increment) {
                $columndef->append("AUTO_INCREMENT");
            }
            if (!$allowNull) {
                $columndef->notNull();
            }

            $columnDefs[$col['id']] = $columndef->__toString();

        }
        if ($hasError) {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_WARNING,
                "message" => "Table creation halted due to error.",
            ];
            $returnvalue = false;
        } else {
            if ($primaryKey != null) {

                $columnDefs[] = "Primary Key (id)";
            }
            $command = $this->db->createCommand();
            try {

                $command->createTable($tablename, $columnDefs)->execute();
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_INFO,
                    "message" => "Table $tablename has been created successfully!",
                ];
            } catch (\Exception $ex) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_ERROR,
                    "message" => "Error occured when creating table $tablename: " . $ex->getMessage(),
                ];
                $returnvalue = false;
            }
        }

        $this->messages[] = [
            "type" => self::MESSAGE_TYPE_INFO,
            "message" => "End Table $tableId creation.",
        ];
        return true;
    }
    private function insertData($data, $tableSchema)
    {
        $tablename = '';

        if ($tableSchema == null) {
            $this->messages[] = [
                "type" => self::MESSAGE_TYPE_WARNING,
                "message" => "There is no table defined for this data. Data are not stored to database .",
            ];
            return false;
        }
        $tablename = $tableSchema->name;

        $records = [];
        $recordInserted = 0;
        foreach ($data as $datum) {
            foreach ($datum as $key => $value) {
                $records[$key] = $value;
            }

            $command = $this->db->createCommand();
            try {
                $command->insert($tablename, $records)->execute();
                $recordInserted++;
            } catch (\Exception $ex) {
                $this->messages[] = [
                    "type" => self::MESSAGE_TYPE_ERROR,
                    "message" => "insertion of Table " . $tablename .
                    " has error: " . $ex->getMessage(),
                ];

            }

        }
        $this->messages[] = [
            "type" => self::MESSAGE_TYPE_INFO,
            "message" => "Number of Record inserted to table $tablename: $recordInserted ",
        ];
        return true;
    }

/**
 * validate name of database , table dan field.
 */
    private function validateName($name)
    {
        $pattern = '/^[a-zA-Z0-9_\$]+$/i';
        return preg_match($pattern, $name) == 1;
    }

    private function createColumnDefs($data)
    {

        $keys = array_keys($data[0]);
        $date_pattern = '/^\d{4}[\-.]\d{1,2}[\-.]\d{1,2}$/';
        $datetime_pattern = '/^\d{4}[\-.]\d{1,2}[\-.]\d{1,2}\s\d{1,2}:\d{1,2}[:\d{1,2}]$/';
        $float_pattern = '/^\d+\.\d+$/i';
        $int_pattern = '/^\d+$/i';
        $boolean_pattern = '/^true|false/i';
        $patterns = ["date" => $date_pattern, "datetime" => $datetime_pattern,
            "float" => $float_pattern, "int" => $int_pattern,
            "bit" => $boolean_pattern];

        $columns = [];
        foreach ($keys as $key) {
            $columns[$key]['id'] = $key;
            $colvalues = \yii\helpers\ArrayHelper::getColumn($data, $key);

            $size = sizeof($colvalues);

            if ($size > 100) {
                $test_array = array_slice($colvalues, 0, 100);
            } else {
                $test_array = $colvalues;
            }

            $coltype = '';
            foreach ($patterns as $pattern => $value) {
                $grep = preg_grep($value, $test_array);
                if (sizeof($grep) == $size) {
                    $coltype = $pattern;
                    break;
                }
            }
            if ($coltype == '') {
                $coltype = 'string';
                $maxlen = max(array_map('strlen', $test_array));
                $coltype .= "(" . $maxlen . ")";
            }
            $columns[$key]['dataType'] = $coltype;
        }
        return $columns;
    }

    private function getColumnType($arr)
    {
        $size = sizeof($arr);
        $size = $size > 100 ? 100 : $size;
        $test_array = array_slice($arr, 0, $size);

        $date_pattern = '/^\d{4}[\-.]\d{1,2}[\-.]\d{1,2}$/';
        $datetime_pattern = '/^\d{4}[\-.]\d{1,2}[\-.]\d{1,2}\s\d{1,2}:\d{1,2}[:\d{1,2}]$/';
        $float_pattern = '/^\d+\.\d+$/i';
        $int_pattern = '/^\d+$/i';
        $boolean_pattern = '/^true|false/i';

        $patterns = ["date" => $date_pattern, "datetime" => $datetime_pattern,
            "float" => $float_pattern, "int" => $int_pattern,
            "bit" => $boolean_pattern];

        $result = '';
        foreach ($patterns as $pattern => $value) {
            $grep = preg_grep($value, $test_array);
            if (sizeof($grep) == $size) {
                $result = $pattern;
                break;
            }

        }

        if ($result == '') {
            $result = 'string';
            $maxlen = max(array_map('strlen', $arr));
            $result .= "(" . $maxlen . ")";
        }

        return $result;

    }

}
