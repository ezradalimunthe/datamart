<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "database_list".
 *
 * @property int $id
 * @property string $database_name Database Name
 * @property string|null $description Description
 * @property string|null $owner Owner
 * @property string $createdAt Created At
 * @property int $state Database State
 */
class DatabaseList extends \yii\db\ActiveRecord
{
    //database state : 0-disabled, 1-available
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'database_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['database_name'], 'required'],
            ['database_name','match','pattern'=>'/^[0-9a-zA-Z$_]+$/i'],
            [['database_name'], 'string', 'max' => 64],
            [['database_name'], 'unique'],
            [['description'], 'string'],
            [['createdAt'], 'safe'],
            [['state'], 'integer'],
         
            [['owner'], 'string', 'max' => 100],
         
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'database_name' => 'Database Name',
            'description' => 'Description',
            'owner' => 'Owner',
            'createdAt' => 'Created At',
            'state' => 'Database State',
        ];
    }

  
    
}
