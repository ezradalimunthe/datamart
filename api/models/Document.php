<?php

namespace api\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;

class Document extends Model
{
  
    public static function getBooks()
    {
        $bookPath = __DIR__ ."/../documentation";
     
        $_basePath = FileHelper::normalizePath($bookPath);
        $alldir = FileHelper::findDirectories($_basePath, ['recursive' => false]);

        $dirnames = array_map(function ($m) {
            return basename($m);
        }, $alldir);
        return $dirnames;
    }

    public static function getPages($book)
    {
        $bookPath =  $bookPath = __DIR__ ."/../documentation";
        $_basePath = FileHelper::normalizePath($bookPath ."//". $book);
        $allfiles = FileHelper::findFiles($_basePath, ['recursive' => false]);
        $dirnames = array_map(function ($m) {
            return basename($m);
        }, $allfiles);
        return $dirnames;
    }

    public static function getPage($book, $page)
    {
        $page .=".md";
        $bookPath = __DIR__ . "/../documentation";
        $_basePath = FileHelper::normalizePath( $bookPath. "//" . $book . "//" . $page);
        
        $content = "# Missing File" . "\n\nPage Not Found";;
        if (file_exists($_basePath)) {
            $content = file_get_contents($_basePath);
        }
        return $content;
    }
}
