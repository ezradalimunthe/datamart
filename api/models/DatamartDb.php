<?php
namespace api\models;

use Yii;

class DatamartDb extends \yii\db\Connection
{
    /**
     * isExist: boolean
     * flag when parameter $createIfNotExist on constructor is false;
     */
    public $isExists = false;
    public $description = '';
    public $owner = '';
    public $createdAt = 'null';
    public function __construct($database_name, $createIfNotExist = true)
    {
        $database_name = "datamart_" . $database_name;
        $dbList = DatabaseList::find()
            ->where(['database_name' => $database_name])
            ->one();

        //get default connection
        $db = Yii::$app->db;
        if ($dbList == null) {
            if ($createIfNotExist) {
                $db->createCommand("CREATE DATABASE " . $database_name . ";")
                    ->execute();
                $dbList = new DatabaseList();
                $dbList->database_name = $database_name;
                $dbList->state = 1;
                $dbList->save();
            } else {
                $this->isExists = false;
                return;
            }

        }

        $db->createCommand("CREATE DATABASE IF NOT EXISTS " . $database_name . ";")
                    ->execute();
        $this->isExists = true;
        $config = self::constructDsn($database_name);
        parent::__construct($config);

    }

    private static function constructDsn($database_name)
    {
        $origninaldsn = Yii::$app->db->dsn;

        $pattern = '/(dbname[\s]*=[\s]*)(?<databasename>[a-zA-Z0-9_$]+)[;]*/i';
        $dsn = preg_replace($pattern, '$1' . $database_name, $origninaldsn);

        $config = [
            'dsn' => $dsn,
            'username' => Yii::$app->db->username,
            'password' => Yii::$app->db->password,
        ];
        return $config;
    }

}
