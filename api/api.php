<?php

namespace api;

/**
 * api module definition class
 */
class api extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'api\v1\controllers';
    public $defaultRoute = "documentation";

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require __DIR__ . '/config/config.php');
     
        $this->setViewPath('@api/v1/views');
        $this->layout= 'main';
    }
}
