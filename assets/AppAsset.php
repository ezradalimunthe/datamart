<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/now-ui-kit.css',
        'css/site.css',
    ];

    public $js = [
        'js/popper.min.js',
        'js/now-ui-kit.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        //'rmrevin\yii\fontawesome\AssetBundle',
        'rmrevin\yii\fontawesome\CdnFreeAssetBundle'
    ];
}
