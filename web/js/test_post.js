var test_data = {
  "id": "covid19_februari",
  "owner": "ezra@halotec-indonesia.com",
  "description": "Data Covid-19 berdasarkan flyer dari Kemenkes",
  "tables": [
    {
      "id": "indonesia",
      "columns": [
        { "id": "record_date", "dataType": "date" },
        { "id": "positif", "dataType": "int" },
        { "id": "sembuh", "dataType": "int" },
        { "id": "meninggal", "dataType": "int" },
        { "id": "aktif", "dataType": "int" },
        { "id": "suspek", "dataType": "int" },
        { "id": "spesimen", "dataType": "int" }
      ],
      "data": [
        {
          "record_date": "2021-02-01",
          "positif": 1089308,
          "sembuh": 8883682,
          "meninggal": 30227,
          "aktif": 175349,
          "suspek": 76343,
          "spesimen": 48213
        },
        {
          "record_date": "2021-02-02",
          "positif": 1099687,
          "sembuh": 896530,
          "meninggal": 30581,
          "aktif": 172675,
          "suspek": 75333,
          "spesimen": 71702
        },
        {
          "record_date": "2021-02-03",
          "positif": 1111671,
          "sembuh": 905665,
          "meninggal": 30770,
          "aktif": 175236,
          "suspek": 76657,
          "spesimen": 74965
        }
      ]
    }
  ]
}

$(function () {
  $("#btnPost").click(function () {
    var postjson = $("#jsonInput").val();
    $("#responseBody").text("posting data...");

    (async () => {
      const rawResponse = await fetch("/api/v1/storage", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: postjson,
      });
      const content = await rawResponse.json();
      $("#responseBody").text(JSON.stringify(content, undefined, 2));
    })();
  });

  $js = JSON.stringify(test_data, undefined, 2);

  $("#jsonInput").val($js);
});
