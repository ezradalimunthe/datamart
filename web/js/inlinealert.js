createAlert=function(message){
    $outer = $("<div>",{
        class:"alert alert-danger",
        role:"alert"
    });
    $container = $("<div>",{
        class:"container"
    });
    $alertIcon = $("<div>",{
        class:"alert-icon"
    }).append($("<i>",{class:"now-ui-icons objects_support-17"}));
   $container.append($alertIcon).append(message)
   $outer.append($container);
   return $outer;
}
