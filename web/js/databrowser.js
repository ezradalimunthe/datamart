$(function () {
  $("a.tablenamelink").click(function (e) {
    e.preventDefault();
    var url = new URL($(this).prop("href"));
    var params = new URLSearchParams(url.search);
    var tablename = params.get("name");

    $("#tablePlaceholder").empty();
    $("#tablePlaceholder").html("Retreiving table structure....");

    var urlschema = pageInfo.requestUrl.tableschema + "/?name=" + tablename;
    var urldata = pageInfo.requestUrl.tabledata + "/?name=" + tablename;

    fetch(urlschema)
      .then((response) => response.json())
      .then((json) => {
        if (json.success == true) {
          createTable(json.data);
          filltableData(json.data, urldata);
        } else {
          $("#tablePlaceholder").empty().append(createAlert(json.data));
        }
      });
  });
});

createTable = function (columnnames) {
  $table = $("<table>", {
    class: "table table-stripped",
    id: "tblrecords",
  });
  $thead = $("<thead>", {});
  $tr = $("<tr>");

  columnnames.forEach((element) => {
    $tr.append(
      $("<th>", {
        text: element,
      })
    );
  });
  $table.append($thead.append($tr));
  $("#tablePlaceholder").empty().append($table);
};

filltableData = function (columnnames, urldata) {
  var featuredef = new Object();
  featuredef.searching = false;
  featuredef.ordering = true;
  featuredef.paging = true;
  featuredef.fixedColumns = true;
  featuredef.columns = new Array();
  columnnames.forEach((element) => {
    featuredef.columns.push({ name: element, data: element });
  });
  featuredef.ajax = urldata;
  featuredef.serverSide = false;
  featuredef.processing = false;

  $("#tablePlaceholder table").DataTable(featuredef);
};
