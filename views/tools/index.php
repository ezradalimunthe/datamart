<?php 
use yii\helpers\Html;
$this->title="Tools";

?>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <?=Html::a("Post json","tools/postjson");?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <?=Html::a("Data Browser","tools/databrowser");?>
            </div>
        </div>
    </div>
</div>