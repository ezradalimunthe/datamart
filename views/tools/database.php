
<?php
use api\models\Encryptor;
use app\assets\DatatableNetAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

DatatableNetAsset::register($this);
$this->registerJsFile('@web/js/databrowser.js',
    ['depends' => [app\assets\DatatableNetAsset::className()]]
);
$this->registerJsFile('@web/js/inlinealert.js',
    ['depends' => [app\assets\DatatableNetAsset::className()]]
);
$this->title = "Database";
$this->params['breadcrumbs'][] = ['label' => 'Data Browser', 'url' => ['tools/databrowser']];
$this->params['breadcrumbs'][] = $databaseName;

$pageInfo = [
    "requestUrl" => [
        "tableschema" => Url::to(["tools/tableschema"]),
        "tabledata" => Url::to(["tools/tabledata"]),

    ],
];

$this->registerJs(
    "var pageInfo=" . \yii\helpers\Json::htmlEncode($pageInfo) . ";",
    View::POS_HEAD,
    'yiiOptions'
);

?>

<h3>Database: <?=$databaseName;?></h3>
<div class="row mb-4">
    <div class="col-sm-3">
        <h5>Tables</h5>
        <div class="list-group">

            <?php
foreach ($tableNames as $tableName) {
    $qparam = implode(";", [
        "db:" . $databaseName,
        "tbl:" . $tableName,
    ]);

    echo Html::a($tableName, ["tools/tableschema", "name" => base64_encode(Encryptor::encrypt($qparam))],
        ["class" => "tablenamelink list-group-item list-group-item-action"]), "\n";
}

?>

        </div>

    </div>
    <div class="col-sm-9">
            <h5>Data</h5>
            <div id="tablePlaceholder" class="table-responsive">
            </div>

        </div>
</div>