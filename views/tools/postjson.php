<?php
$this->title = " Index ";

$this->registerJsFile(
   '@web/js/test_post.js',
   ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="api-default-index mt-4">
   <h2>Post Json</h2>
   <h4>Post Json data </h4>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="jsonInput">Json</label>
            <textarea class="form-control border" id="jsonInput" autocomplete="off"
                style='height:400px;max-height:400px!important;white-space:pre;oveflow:auto;resize:horizontal'
                rows="20"></textarea>
        </div>
    </div>
    <div class="col-sm-6">
      <pre id = "responseBody" class="border" style='height:400px;overflow:horizontal;'>
      </pre>
    </div>
</div>

<button class="btn btn-primary" id="btnPost">Post</button>