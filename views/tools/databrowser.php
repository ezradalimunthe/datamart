<?php
use api\models\Encryptor;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
$this->title = "Data Browser";

$this->registerJsFile('@web/js/databrowser.js',
    ['depends' => [app\assets\DatatableNetAsset::className()]]
);

$pageInfo = [
    "requestUrl" => [
        "tableschema" => Url::to(["tools/tableschema"]),
        "tabledata" => Url::to(["tools/tabledata"]),

    ],
];

$this->registerJs(
    "var pageInfo=" . \yii\helpers\Json::htmlEncode($pageInfo) . ";",
    View::POS_HEAD,
    'yiiOptions'
);

$this->params['breadcrumbs'][] = $this->title;

?>

<h3>Data Browser</h3>

<div class="row">

    <?php
foreach ($databases as $database) {
    $databasename = str_replace("datamart_", "", $database->database_name)
    ?><div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2 bg-light">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                            <?=$databasename;?>
                        </div>
                        <dl class="row">
                            <dt class="col-sm-4">Created</dt>
                            <dd class="col-sm-8"><?=$database->createdAt;?></dd>
                            <dt class="col-sm-4">Owner</dt>
                            <dd class="col-sm-8"> <?=$database->owner;?></dd>
                            <dt class="col-sm-4">Description</dt>
                            <dd class="col-sm-8"> <?=$database->description;?></dd>
                        </dl>

                        <?=Html::a("Open", ["tools/database", "name" => base64_encode(Encryptor::encrypt($databasename))],
        ["class" => "databasenamelink btn btn-primary"]), "\n";?>
                    </div>
                    <div class="col-auto">
                        <?=FAS::icon("database", ["class" => 'text-gray'])->size(FAS::SIZE_3X);?>
                    </div>
                </div>




            </div>
        </div>
    </div>
    <?php
}
?>


</div>