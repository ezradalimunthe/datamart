<?php
use yii\web\View;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;
$this->title = "Database Not Found!";

?>
<div class="row">
    <div class="col-sm-12">
        <div class="error-template">
            <h1>
                Oops!</h1>
            <h2>
                Database Not Found!</h2>
            <div class="error-details">
                Sorry, Database <?=$databaseName;?> doesn't exist
            </div>
            <div class="error-actions">
                <?=Html::a(FAS::icon("database") . " Databrowser",["tools/databrowser"],
            ["class"=>"btn btn-primary btn-lg"]);?>
                <?=Html::a(FAS::icon("envelope") . " Contact Support",["site/contact"],
            ["class"=>"btn btn-primary btn-lg"]);?>


            </div>
        </div>
    </div>
</div>