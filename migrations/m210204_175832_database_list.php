<?php

use yii\db\Migration;

/**
 * Class m210204_175832_database_list
 */
class m210204_175832_database_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('database_list', [
            'id' => $this->primaryKey(),
            'database_name' => $this->string(64)->notNull()->unique(),
            'description' => $this->text(),
            'owner'=>$this->string(100),
            'createdAt'=> $this->timestamp()->notNull(),
            'state'=>$this->integer()->defaultValue(1)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('database_list');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210204_175832_database_list cannot be reverted.\n";

        return false;
    }
    */
}
